﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Configuration;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTests.Tests
{
    [TestClass]
    public abstract class TestBase
    {
        public static FirefoxDriver _driver;
        public static string _baseUrl;

        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            var driverService = FirefoxDriverService.CreateDefaultService();
            driverService.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
            driverService.HideCommandPromptWindow = true;
            driverService.SuppressInitialDiagnosticInformation = true;

            _driver = new FirefoxDriver(driverService, new FirefoxOptions(), TimeSpan.FromSeconds(240));

            _driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(240));

            _baseUrl = "https://jernhusen-test.winston.se";
        }

        [AssemblyCleanup]
        public static void AssemblyCleanUp()
        {
            _driver.Quit();
        }

        public static void WaitForElementLoad(By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(timeoutInSeconds));
                wait.Until(ExpectedConditions.ElementIsVisible(by));
            }
        }
    }
}
