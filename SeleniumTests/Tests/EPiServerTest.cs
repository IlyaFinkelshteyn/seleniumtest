﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Configuration;
using System.Drawing.Imaging;

namespace SeleniumTests.Tests
{
    [TestClass]
    public class EPiServerTest : TestBase
    {
        [TestInitialize]
        public void TestInit()
        {
            _driver.Navigate().GoToUrl(_baseUrl + "/EPiServer/CMS/#context=epi.cms.contentdata:///1&viewsetting=viewlanguage:///sv");
        }

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            EPiServerLogin();
        }

        public static void EPiServerLogin()
        {
            _driver.Navigate().GoToUrl(_baseUrl + "/util/login.aspx?ReturnUrl=%2FEPiServer%2FCMS%2F");

            var username = "SeleniumTestUser";
            var password = "zXP6Xl3mpME0!";

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return;

            WaitForElementLoad(By.Id("LoginControl_UserName"), 120);

            var usernameField = _driver.FindElement(By.Id("LoginControl_UserName"));
            usernameField.Clear();
            usernameField.SendKeys(username);

            var passwordField = _driver.FindElement(By.Id("LoginControl_Password"));
            passwordField.Clear();
            passwordField.SendKeys(password + Keys.Enter);

            WaitForElementLoad(By.Id("globalMenuContainer"), 120);
        }

        [TestCleanup]
        public void TestCleanUp()
        {
        }

        [TestMethod]
        public void BackofficeTitleIsEPiServer()
        {
            var titleText = _driver.FindElement(By.XPath("//title")).Text;

            StringAssert.Contains(titleText, "EPiServer CMS");

            _driver.GetScreenshot().SaveAsFile(AppDomain.CurrentDomain.BaseDirectory + "\\..\\..\\Screenshots\\firefox-episerveredit-screenshot.png", ImageFormat.Png);
        }

        [TestMethod]
        public void LicenseErrorNotShown()
        {
            WaitForElementLoad(By.Id("globalMenuContainer"), 30);

            var globalMenu = _driver.FindElement(By.Id("globalMenuContainer")).Text;

            Assert.IsFalse(globalMenu.Contains("license error"));
        }
    }
}