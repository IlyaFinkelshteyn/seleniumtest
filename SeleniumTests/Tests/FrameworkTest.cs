﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Configuration;

namespace SeleniumTests.Tests
{
    [TestClass]
    public class FrameworkTest : TestBase
    {
        [TestInitialize]
        public void TestInit()
        {
            _driver.Navigate().GoToUrl(_baseUrl);
        }

        [TestCleanup]
        public void TestCleanUp()
        {
        }

        [TestMethod]
        public void MetadataTextIsShown()
        {
            var titleText = _driver.FindElement(By.XPath("//title")).Text;
            StringAssert.Contains(titleText, "Jernhusen");

            var metaDescriptionText = _driver.FindElement(By.XPath("//meta[@name='description']")).GetAttribute("content");
            Assert.IsNotNull(metaDescriptionText);
            Assert.AreNotEqual(string.Empty, metaDescriptionText);
        }

        [TestMethod]
        public void MainNavigationContainsThreeButtons()
        {
            var buttons = _driver.FindElement(By.ClassName("nav-header-index")).FindElements(By.TagName("button"));
            Assert.AreEqual(3, buttons.Count);
        }
    }
}