﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System.Drawing.Imaging;

namespace SeleniumTests.Tests
{
    [TestClass]
    public class StartpageTest : TestBase
    {
        [TestInitialize]
        public void TestInit()
        {
            _driver.Navigate().GoToUrl(_baseUrl);
        }

        [TestCleanup]
        public void TestCleanUp()
        {
        }

        [TestMethod]
        public void CssClassOnBodyIsSet()
        {
            var bodyClass = _driver.FindElement(By.TagName("body")).GetAttribute("class");
            Assert.AreEqual("start", bodyClass);

            _driver.GetScreenshot().SaveAsFile(AppDomain.CurrentDomain.BaseDirectory + "\\..\\..\\Screenshots\\firefox-startpage-screenshot.png", ImageFormat.Png);
        }

        //[TestMethod]
        //public void JombotronTextIsShown()
        //{
        //    var jombotronText = _driver.FindElement(By.TagName("h1")).Text;
        //    Assert.IsNotNull(jombotronText);
        //    Assert.AreNotEqual(string.Empty, jombotronText);
        //}
    }
}