$success = $true 

vstest.console /logger:Appveyor .\SeleniumTests\bin\Release\SeleniumTests.dll
if($LastExitCode -ne 0) {$success = $false}

$screenshots_path = "$env:appveyor_build_folder\SeleniumTests\Screenshots\*.png"
Get-ChildItem $screenshots_path | % { Push-AppveyorArtifact $_.FullName -FileName $_.Name }

if (!$success) {throw "at least one test failed"}
#test23